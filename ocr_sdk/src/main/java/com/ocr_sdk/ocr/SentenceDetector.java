package com.ocr_sdk.ocr;

import android.graphics.Color;
import android.support.annotation.NonNull;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.method.MovementMethod;
import android.text.style.ClickableSpan;
import android.view.View;
import android.widget.TextView;

import java.text.BreakIterator;
import java.util.Locale;
/**
 * @author Created by Abhijit Rao on 2017/3/20.
 */
public class SentenceDetector {

    private TextView textView;

    public interface OnClickListener {
        void onClick(String word);
    }


    public static SentenceDetector getInstance() {
        return new SentenceDetector();
    }

    public SentenceDetector setTextView(TextView textView) {
        this.textView = textView;
        return this;
    }

    public void processSentence(String sentence, OnClickListener listener) {
        SpannableString link = makeLinkSpanForSentence(sentence, listener);

        if (textView != null) {

            // Set the TextView's text
//        textView.setText("To perform action, ");

            // Append the link we created above using a function defined below.
            textView.append(link);

            // Append a period (this will not be a link).
//        tv.append(".");

            // This line makes the link clickable!
            makeLinksFocusable(textView);
        }
    }


    private SpannableString makeLinkSpanForSentence(String result, OnClickListener listener) {
        SpannableString spans = new SpannableString(result);
        BreakIterator iterator = BreakIterator.getWordInstance(Locale.US);
        try {
            if (!TextUtils.isEmpty(result)) {
                iterator.setText(result);
                int start = iterator.first();
                for (int end = iterator.next(); end != BreakIterator.DONE; start = end, end = iterator.next()) {
                    String currentWord = result.substring(start, end);
                    if (Character.isLetterOrDigit(currentWord.charAt(0))) {
                        spans.setSpan(new ClickableString(currentWord, listener), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return spans;
    }

    private SpannableString makeLinkSpanForWord(String text, OnClickListener listener) {
        SpannableString link = new SpannableString(text);
        link.setSpan(new ClickableString(text, listener), 0, text.length(),
                SpannableString.SPAN_INCLUSIVE_EXCLUSIVE);
        return link;
    }

    private void makeLinksFocusable(TextView tv) {
        MovementMethod m = tv.getMovementMethod();
        if (m == null || !(m instanceof LinkMovementMethod)) {
            if (tv.getLinksClickable()) {
                tv.setMovementMethod(LinkMovementMethod.getInstance());
            }
        }
    }

//    private ClickableSpan getClickableSpan(final String word) {
//        return new ClickableSpan() {
//            final String mWord;
//
//            {
//                mWord = word;
//            }
//
//            @Override
//            public void onClick(View widget) {
//                checkWord(mWord);
//            }
//
////            public void updateDrawState(TextPaint ds) {
////                super.updateDrawState(ds);
////                ds.setUnderlineText(false);
////                ds.setColor(isDayMode ? Color.BLACK : Color.WHITE);
////            }
//        };
//    }

    /*
     * ClickableString class
     */

    private static class ClickableString extends ClickableSpan {
        private final String word;
        private OnClickListener mListener;

        public ClickableString(String word, OnClickListener listener) {
            this.word = word;
            this.mListener = listener;
        }

        @Override
        public void onClick(View v) {
            mListener.onClick(word);
        }

        @Override
        public void updateDrawState(@NonNull TextPaint ds) {
            super.updateDrawState(ds);
            ds.setUnderlineText(false);
            ds.setColor(Color.parseColor("#000000"));
        }
    }
}

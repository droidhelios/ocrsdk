package com.ocr_sdk.ocr;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Handler;
import android.util.SparseArray;

import com.google.android.gms.vision.Frame;
import com.google.android.gms.vision.text.Text;
import com.google.android.gms.vision.text.TextBlock;
import com.google.android.gms.vision.text.TextRecognizer;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
/**
 * @author Created by Abhijit Rao on 2017/3/20.
 */
public class TextDetector {
    private final Context context;
    private StringBuilder lines;
    private List<String> mLines;
    private List<String> mWords;
    private Listener listener;
    private final Handler mHandler;
    private boolean isCharacter;
    private int minWordLength;

    public interface Listener {
        void onSuccess(StringBuilder result);

        void onDetectedLines(List<String> lines);

        void onDetectedWords(List<String> words);

        void onFailure(Exception e);
    }

    private TextRecognizer detector;


    public TextDetector addListener(Listener listener) {
        this.listener = listener;
        return this;
    }

    public TextDetector setCharacterOnly(boolean isCharter) {
        this.isCharacter = isCharter;
        return this;
    }

    public TextDetector setMinWordLength(int minWordLength) {
        this.minWordLength = minWordLength;
        return this;
    }

    private TextDetector(Context context) {
        this.detector = new TextRecognizer.Builder(context).build();
        this.mHandler = new Handler();
        this.context = context;
        this.isCharacter = false;
        this.minWordLength = 1;
    }

    public static TextDetector getInstance(Context context) {
        return new TextDetector(context);
    }



    public void processImageFromUri(Uri uri) {
        try {
            Bitmap bitmap = decodeBitmapUri(context, uri);
            processImageFromBitmap(bitmap);
        } catch (final FileNotFoundException e) {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    if (listener != null) {
                        listener.onFailure(new Exception(e.getMessage()));
                    }
                }
            });
        }
    }
    public void processImageFromBitmap(final Bitmap bitmap) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                lines = new StringBuilder();
                mLines = new ArrayList<>();
                mWords = new ArrayList<>();
                try {
                    if (detector.isOperational() && bitmap != null) {
                        Frame frame = new Frame.Builder().setBitmap(bitmap).build();
                        SparseArray<TextBlock> textBlocks = detector.detect(frame);
                        for (int index = 0; index < textBlocks.size(); index++) {
                            //extract scanned text blocks here
                            TextBlock tBlock = textBlocks.valueAt(index);
                            for (Text line : tBlock.getComponents()) {
                                //extract scanned text lines here
                                lines.append(line.getValue() + "\n");
                                mLines.add(line.getValue());
                                for (Text element : line.getComponents()) {
                                    //extract scanned text words here
                                    if (isCharacter) {
                                        if (Character.isLetter(element.getValue().charAt(0)) && element.getValue().length() >= minWordLength) {
                                            mWords.add(element.getValue());
                                        }
                                    } else {
                                        if (element.getValue().length() >= minWordLength) {
                                            mWords.add(element.getValue());
                                        }
                                    }
                                }
                            }
                        }

                        if (textBlocks.size() == 0) {
                            throw new Exception("Scan Failed: Found nothing to scan");
                        } else {
                            mHandler.post(new Runnable() {
                                @Override
                                public void run() {
                                    if (listener != null) {
                                        listener.onSuccess(lines);
                                        listener.onDetectedLines(mLines);
                                        listener.onDetectedWords(mWords);
                                    }
                                }
                            });
                        }
                    } else {
                        throw new Exception("Could not set up the detector!");
                    }
                } catch (final Exception e) {
                    mHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            if (listener != null) {
                                listener.onFailure(e);
                            }
                        }
                    });
                }
            }
        }).start();
    }


    private Bitmap decodeBitmapUri(Context ctx, Uri uri) throws FileNotFoundException {
        int targetW = 600;
        int targetH = 600;
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(ctx.getContentResolver().openInputStream(uri), null, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        int scaleFactor = Math.min(photoW / targetW, photoH / targetH);
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;

        return BitmapFactory.decodeStream(ctx.getContentResolver()
                .openInputStream(uri), null, bmOptions);
    }
}

//    Frame frame = new Frame.Builder().setBitmap(bitmap).build();
//                        SparseArray<TextBlock> textBlocks = detector.detect(frame);
//                        String blocks = "";
//                        String lines = "";
//                        String words = "";
//                        for (int index = 0; index < textBlocks.size(); index++) {
//                            //extract scanned text blocks here
//                            TextBlock tBlock = textBlocks.valueAt(index);
//                            blocks = blocks + tBlock.getValue() + "\n" + "\n";
//                            for (Text line : tBlock.getComponents()) {
//                                //extract scanned text lines here
//                                lines = lines + line.getValue() + "\n";
//                                for (Text element : line.getComponents()) {
//                                    //extract scanned text words here
//                                    words = words + element.getValue() + ", ";
//                                }
//                            }
//                        }


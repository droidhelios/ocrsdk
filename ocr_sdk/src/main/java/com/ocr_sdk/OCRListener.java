package com.ocr_sdk;

import java.util.List;
/**
 * @author Created by Abhijit Rao on 2017/3/20.
 */
public interface OCRListener {

    void onSuccess(StringBuilder result);

    void onDetectedLines(List<String> lines);

    void onDetectedWords(List<String> words);

    void onFailure(Exception e);
}

package com.ocr_sdk.camera;

import android.net.Uri;

/**
 * @author Created by Abhijit Rao on 2017/3/20.
 */
public interface CamListener {

    void onSuccess(Uri result);
    void onFailure(Exception e);
}

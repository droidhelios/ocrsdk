package com.ocr_sdk.camera;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;

/**
 * @author Created by Abhijit Rao on 2017/3/20.
 */
public class CamCropper {

    private static CamCropper _instance;
    private final Context context;
    private CamListener listener;

    public CamCropper(Context context) {
        this.context = context;

    }

    public static CamCropper newInstance(Context context) {
        _instance = new CamCropper(context);
        return _instance;
    }

    static CamCropper getInstance(Context context) {
        if(_instance == null){
            _instance = new CamCropper(context);
        }
        return _instance;
    }

    public CamCropper addListener(CamListener listener) {
        this.listener = listener;
        return this;
    }


    public CamCropper startCropping() {
        Intent intent = new Intent(context, CropActivity.class);
//        intent.putExtra(CameraConfig.RATIO_WIDTH, 855);
//        intent.putExtra(CameraConfig.RATIO_HEIGHT, 541);
        intent.putExtra(CameraConfig.RATIO_WIDTH, 4);
        intent.putExtra(CameraConfig.RATIO_HEIGHT, 1);
        intent.putExtra(CameraConfig.PERCENT_LARGE, 0.8f);
        intent.putExtra(CameraConfig.MASK_COLOR, 0x2f000000);
        intent.putExtra(CameraConfig.TOP_OFFSET, 0);
        intent.putExtra(CameraConfig.RECT_CORNER_COLOR, 0xff00ff00);
        intent.putExtra(CameraConfig.TEXT_COLOR, 0xffffffff);
        intent.putExtra(CameraConfig.HINT_TEXT, CameraConfig.DEFAULT_HINT_TEXT);
        intent.putExtra(CameraConfig.IMAGE_PATH, Environment.getExternalStorageDirectory().getAbsolutePath() + "/CameraCardCrop/" + System.currentTimeMillis() + ".jpg");
        context.startActivity(intent);
        return this;
    }


    public void showFailure(Exception e) {
        if(listener!=null){
            listener.onFailure(e);
        }
    }

    public void processUri(Uri parse) {
        if(listener!=null){
            listener.onSuccess(parse);
        }
    }
}

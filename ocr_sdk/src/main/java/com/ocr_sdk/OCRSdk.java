package com.ocr_sdk;

import android.content.Context;
import android.net.Uri;
import android.view.View;
import android.widget.TextView;

import com.ocr_sdk.camera.CamCropper;
import com.ocr_sdk.camera.CamListener;
import com.ocr_sdk.ocr.SentenceDetector;
import com.ocr_sdk.ocr.TextDetector;

import java.util.List;
/**
 * @author Created by Abhijit Rao on 2017/3/20.
 */
public class OCRSdk {
    private static OCRSdk _instance;
    private final Context context;
    private final CamCropper camCropper;
    private final TextDetector textDetector;
    private final SentenceDetector sentenceDetector;
    private OCRListener listener;
    private TextView textView;
    private SentenceDetector.OnClickListener wordClickListener;

    public OCRSdk(Context context) {
        this.context = context;
        this.camCropper = CamCropper.newInstance(context);
        this.textDetector = TextDetector.getInstance(context)
                .setCharacterOnly(true)
                .setMinWordLength(2);
        this.sentenceDetector = SentenceDetector.getInstance();

    }

    public static OCRSdk newInstance(Context context) {
        _instance = new OCRSdk(context);
        return _instance;
    }

    static OCRSdk getInstance(Context context) {
        if (_instance == null) {
            _instance = new OCRSdk(context);
        }
        return _instance;
    }

    public OCRSdk addListener(OCRListener cameraCropListener) {
        this.listener = cameraCropListener;
        return this;
    }


    public OCRSdk addTextView(TextView textView, SentenceDetector.OnClickListener wordClickListener) {
        this.textView = textView;
        this.wordClickListener = wordClickListener;
        return this;
    }


    public OCRSdk startScanning() {
        camCropper.addListener(new CamListener() {
            @Override
            public void onSuccess(Uri result) {
                processDocumentImage(result);
            }

            @Override
            public void onFailure(Exception e) {
                showFailure(e);
            }
        }).startCropping();
        return this;
    }


    public void showFailure(Exception e) {
        if (listener != null) {
            listener.onFailure(e);
        }
    }

    private void processDocumentImage(Uri uri) {
        if (listener == null) {
            showFailure(new Exception("Listener not registered"));
            return;
        }
        textDetector.addListener(new TextDetector.Listener() {
            @Override
            public void onSuccess(StringBuilder result) {
                setResultOnTextView(result);
                listener.onSuccess(result);
            }

            @Override
            public void onDetectedLines(List<String> lines) {
                listener.onDetectedLines(lines);

            }

            @Override
            public void onDetectedWords(List<String> words) {
                listener.onDetectedWords(words);
            }

            @Override
            public void onFailure(Exception e) {
                listener.onFailure(e);
            }
        }).processImageFromUri(uri);
    }

    private void setResultOnTextView(StringBuilder result) {
        if (textView != null) {
            textView.setVisibility(View.VISIBLE);
            textView.setText("");
            sentenceDetector.setTextView(textView)
                    .processSentence(result.toString(), wordClickListener);
        }
    }

}

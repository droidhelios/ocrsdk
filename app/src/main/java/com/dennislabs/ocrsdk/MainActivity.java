package com.dennislabs.ocrsdk;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.ocr_sdk.OCRListener;
import com.ocr_sdk.OCRSdk;

import java.util.List;

/**
 * @author Abhijit Rao Created on 2017/3/20.
 */
public class MainActivity extends AppCompatActivity {

    private TextView tvStatus;
    private OCRSdk scanner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tvStatus = findViewById(R.id.tvStatus);

        scanner = OCRSdk.newInstance(this).addListener(new OCRListener() {
            @Override
            public void onSuccess(StringBuilder result) {
                tvStatus.setText(result);
            }

            @Override
            public void onDetectedLines(List<String> lines) {

            }

            @Override
            public void onDetectedWords(List<String> words) {

            }

            @Override
            public void onFailure(Exception e) {

            }
        }).startScanning();
    }

    public void onStartScanning(View view) {
        scanner.startScanning();
    }
}

# OCR Sdk
Scan text from camera
  
## Setup

Add this to your project build.gradle
``` gradle
allprojects {
    repositories {
        maven { url 'https://jitpack.io' }
    }
}
```
#### Dependency
[![](https://jitpack.io/v/org.bitbucket.droidhelios/ocrsdk.svg)](https://jitpack.io/#org.bitbucket.droidhelios/ocrsdk)
```gradle
dependencies {
    implementation 'org.bitbucket.droidhelios:ocrsdk:x.y'
}
```
 
### Usage 
In your Activity class:
#### Initialization method
```java 
     scanner = OCRSdk.newInstance(this).addListener(new OCRListener() {
            @Override
            public void onSuccess(StringBuilder result) {
                tvStatus.setText(result);
            }

            @Override
            public void onDetectedLines(List<String> lines) {

            }

            @Override
            public void onDetectedWords(List<String> words) {

            }

            @Override
            public void onFailure(Exception e) {

            }
        });
        scanner.addTextView(tvStatus, new SentenceDetector.OnClickListener() {
            @Override
            public void onClick(String word) {

            }
        });

        scanner.startScanning();
```
 